from django.db import models
import datetime

# Create your models here.

class Profil(models.Model):
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('Other', 'Other')
    )

    photo = models.URLField(default="https://image.ibb.co/ippEdw/potopropil.png")
    name = models.CharField(max_length=30, default='Tuan Junahedi')
    bday = models.DateField(default=datetime.date(2017,10,5))
    gender = models.CharField(max_length=6, choices=GENDER_CHOICES, default='Male')
    expertise = models.TextField(max_length=60, default='Exist; Sentient; Real;')
    description = models.TextField(max_length=120, default='A person created by the will of completing TP 1 PPW')
    email = models.EmailField(default='tujuh@yahooo.com')

    def __str__(self):
        return self.name