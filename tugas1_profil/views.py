from django.shortcuts import render
from .models import Profil
from django.http import HttpResponseRedirect


# Create your views here.
user = Profil.objects.get_or_create(pk=1)
user_name = str(user[0])
response = {}
def index(request):
    response['user'] = user[0]
    return render(request, 'profile.html', response)

