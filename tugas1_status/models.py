from django.db import models

class Status(models.Model):
    text = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now_add=True)