from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status
from .models import Status
from .forms import Status_Form

class Lab5UnitTest(TestCase):

    def test_status_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_status_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        new_activity = Status.objects.create(text='Hi, this is my status')

        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'text' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.error_messages['required'],
            'Status tidak boleh kosong.'
        )
    
    def test_form_validation_for_long_text(self):
        test = "";
        for i in range(110):
            test += "a"
        form = Status_Form(data={'text' : test})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.error_messages['invalid'],
            'Status terlalu panjang.'
        )
    
    def test_form_validation_for_valid_text(self):
        test = "Hi, this is my status"
        form = Status_Form(data={'text' : test})
        self.assertTrue(form.is_valid())

    def test_status_post_success_and_render_the_result(self):
        test = 'Hi, this is my status'
        response_post = Client().post('/add_status', {'text' : test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_status_post_fail_and_render_the_result(self):
        test = "";
        for i in range(110):
            test += "a"
        response_post = Client().post('/add_status', {'text' : test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)