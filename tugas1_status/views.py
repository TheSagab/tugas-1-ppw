from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status
from tugas1_profil.views import user_name

# Create your views here.
response = {}
def index(request):
    response['author'] = user_name
    status = Status.objects.all()
    response['status'] = status
    response['status_form'] = Status_Form
    html = 'status/status.html'
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['text'] = request.POST['text']
        status = Status(text=response['text'])
        status.save()
    return HttpResponseRedirect('/')
