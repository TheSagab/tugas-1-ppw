from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, is_valid_link, delete_friend
from .models import Friend
from .forms import Friends_Form
# Create your tests here.
class FriendPageUnitTest(TestCase):
    def test_friend_url_is_exist(self):
        response = Client().get('/friends/')
        self.assertEqual(response.status_code, 200)

    def test_friend_using_index_func(self):
        found = resolve('/friends/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_friend(self):
        #Creating a new activity
        new_friend = Friend.objects.create(name="TheSagab",profile_link="http://sagab-ppw-app.herokuapp.com/lab-4/")

        #Retrieving all available activity
        counting_all_available_friend= Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend,1)

    def test_form_friend_input_has_placeholder_and_css_classes(self):
        form = Friends_Form()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('<label for="id_name">Name:</label>', form.as_p())
        self.assertIn('<label for="id_friendURL">URL:</label>', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Friends_Form(data={'name': '', 'friendURL': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
    
    def test_friend_post_success_and_render_the_result(self):
        name = 'Sagab'
        profile_link = 'http://sagab-ppw-app.herokuapp.com/'
        response_post = Client().post('/friends/friend_post', {'name': name, 'friendURL': profile_link})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/friends/')
        html_response = response.content.decode('utf8')
        self.assertIn(name, html_response)
        self.assertIn(profile_link, html_response)

    def test_friend_post_fail_and_render_the_result(self):
        name = 'Sagab'
        profile_link='lksvkjrbvkjerbvrifiu'
        response_post = Client().post('/friends/friend_post', {'name': name, 'friendURL': profile_link})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/friends/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(name, html_response)
        self.assertNotIn(profile_link, html_response)

    def test_friend_valid_link(self):
        profile_link="http://tujuh.herokuapp.com/"
        self.assertTrue(is_valid_link(profile_link))

        invalid_link="http://lulxDgaadaHAHAHAHA.herokuapp.com/"
        self.assertFalse(is_valid_link(invalid_link))

    def test_friend_delete_(self):
        friend = Friend.objects.create(name="TheSagab",profile_link="http://sagab-ppw-app.herokuapp.com/")
        delete_friend('/friends/delete_todo/'+str(friend.id), friend.id)

        count_item = Friend.objects.all().count()
        self.assertEqual(count_item,0)
