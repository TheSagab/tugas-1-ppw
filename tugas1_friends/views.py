from django.shortcuts import render
from .forms import Friends_Form
from .models import Friend
import urllib.request
import urllib.error
from django.http import HttpResponseRedirect
# Create your views here.

response = {}
def index(request, success = True):
    friend = Friend.objects.all()
    response['title'] = 'Friends'
    response['form'] = Friends_Form
    response['list'] = friend
    response['success'] = success
    return render(request, 'friends.html', response)

def friend_post(request):
    form = Friends_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        friend = Friend(name=request.POST['name'], profile_link=request.POST['friendURL'])
        if (is_valid_link(request.POST['friendURL'])):
            success = True
            friend.save()
    else:
        success = False
    return index(request, success)

def is_valid_link(link):
    try:
        link_test = urllib.request.urlopen(url=link)
        return True
    except:
        return False

def delete_friend(request, id = None):
    friend = Friend.objects.get(id = id)
    friend.delete()
    return HttpResponseRedirect('/friends')
