from django.conf.urls import url
from .views import index, friend_post, delete_friend

#url for app, add your URL Configuration

urlpatterns = [
#TODO Implement this
    url(r'^$', index, name='index'),
    url(r'^friend_post', friend_post, name='friend_post'),
    url(r'^delete_friend/(\d+)/$', delete_friend, name = 'delete_friend'),
]
