from django import forms

class Friends_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan benar',
    }
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukan nama teman'
    }
    url_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukan URL teman, awali dengan http://'
    }
    name = forms.CharField(label='Name', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    friendURL = forms.URLField(label='URL', required=True, widget=forms.URLInput(attrs=url_attrs))
