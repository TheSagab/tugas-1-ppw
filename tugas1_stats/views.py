from django.shortcuts import render
from tugas1_profil.views import user_name
from tugas1_friends.models import Friend
from tugas1_status.models import Status

#TODO Implement
name = user_name

def index(request):
    response = {'title':'Statistics','name':name,'friends':hitungFriends(),'feed':hitungFeeds(),'status':Status.objects.last()}
    return render(request, 'stats.html',response)

def hitungFriends():
    return len(Friend.objects.all())

def hitungFeeds():
    return len(Status.objects.all())
