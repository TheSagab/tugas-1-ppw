from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index,hitungFriends,hitungFeeds
from django.http import HttpRequest
from tugas1_friends.models import Friend
from tugas1_status.models import Status

# Create your tests here.
class StatsUnitTest(TestCase):

    def test_stats_url_is_exist(self):
        response = Client().get('/stats/')
        self.assertEqual(response.status_code, 200)

    def test_stats_using_index_func(self):
        found = resolve('/stats/')
        self.assertEqual(found.func, index)

    def test_feeds_increasing(self):
        # Creating a new activity
        status = Status(text='test ppw')
        status.save()

        # Retrieving all available activity
        counting_all_available_status = hitungFeeds()
        self.assertEqual(counting_all_available_status, 1)

    def test_friends_increasing(self):
        # Creating a new activity
        friend = Friend(name='test ppw', profile_link='http://tujuh.herokuapp.com')
        friend.save()

        # Retrieving all available activity
        counting_all_available_friend = hitungFriends()
        self.assertEqual(counting_all_available_friend, 1)
